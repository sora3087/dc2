app.controller('Main', ['$scope', '$element', function($scope, $element){
  $scope.navItems = [
    {
      path: '/profile',
      name: 'Profile',
      tooltip: 'Profile',
      icon: 'cache/profileIcons/64b7ea4cc6b2b3c768a8f57e26d5af0f270fbce6_full.jpg'
    },
    {
      path: '/library',
      name: 'Library',
      tooltip: 'Library',
      icon: 'fa-gamepad'
    },
    {
      path: '/store',
      name: 'Store',
      tooltip: 'Store',
      icon: 'fa-shopping-bag'
    },
    {
      path: '/community',
      name: 'Community',
      tooltip: 'Community',
      icon: 'fa-comments'
    },
    {
      path: '/settings',
      name: 'Settings',
      tooltip: 'Settings',
      icon: 'fa-cog'
    }
  ];
  $scope.fontClass = '';
}]);