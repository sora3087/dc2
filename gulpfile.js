const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const bulkSass = require('gulp-sass-bulk-import');
// const minify = require('gulp-uglify');
const babel = require('gulp-babel');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('default', ['scripts', 'styles']);

var basePath = __dirname+"\\";
var inputJS = basePath+"src\\js\\";
var inputSCSS = basePath+"src\\scss\\";
var outputJS = basePath+"assets\\js\\";
var outputCSS = basePath+"assets\\css\\";

console.log(basePath, inputJS, inputSCSS, outputJS, outputCSS);

gulp.task('scripts', function(){
  return gulp.src([
      inputJS+'app.js',
      inputJS+'config\\*.config.js',
      inputJS+'controllers\\*.controller.js',
      inputJS+'directives\\*.directive.js',
    ])
    .pipe(sourcemaps.init())
    .pipe(babel({presets: ['env']}))
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(outputJS));
});

gulp.task('styles', function(){
  return gulp.src([inputSCSS+"*.scss"])
    .pipe(sourcemaps.init())
    .pipe(bulkSass())
    .pipe(sass({outputStyle: "expanded"}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(outputCSS));
});

gulp.task('watch', ['scripts', 'styles', 'watch-scripts', 'watch-styles']);

gulp.task('watch-scripts', function(){
  gulp.watch(inputJS+"**\\*.js", function(){
    gulp.start('scripts');
  });
});

gulp.task('watch-styles', function(){
  gulp.watch(inputSCSS+"**/*.scss", function(){
    gulp.start('styles');
  });
});