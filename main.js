const {app, BrowserWindow, ipcMain} = require('electron');
const path = require('path');
const url = require('url');
const vdf = require('./vdf');
const SteamCMD = require('./steam-cmd-wrapper');
var steam = new SteamCMD(__dirname+'/bin/');

let win;
let installPath;

function createMainWindow () {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 600,
    frame: false,
    show: false,
    backgroundColor: '#fff',
    title: "Dreamcast 2"
  });

  // and load the index.html of the app.
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'pages/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Open the DevTools.

  win.on('ready-to-show', () => {
    win.show();
    win.webContents.openDevTools();
  });
  
  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null
  });
}

function createLogin(){
  var modal = new BrowserWindow({
    width: 550,
    height: 350,
    frame: false,
    show: false,
    backgroundColor: '#fff',
    resizable: false,
    movable: false,
    minimizable: false,
    maximizable: false,
    title: "Dreamcast 2"
  });

  modal.on('ready-to-show', () => {
    modal.show();
    // steam.login().then(()=>{

    // });
    ipcMain.on('login', (e, user) => {
      steam.login(user.name, user.password)
      .then(() => {
        console.log('main: logged in');
        e.sender.send('logged-in');
        createMainWindow();
        closeLogin();
      }, (errorCode) => {
        console.log(errorCode);
        if (errorCode == 1){
          e.sender.send('wrong-password');
        }else if(errorCode == 2){
          e.sender.send('need-code');
        }else{
          e.sender.send('login-error'); //probably not a valid user
        }
      })
    });
    ipcMain.on('guard', (e, code) => {
      steam.enterCode(code).then(()=>{
        e.sender.send('logged-in');
        createMainWindow();
        closeLogin();
      }, () => {
        e.sender.send('guard-failed');
      });
    });

    function closeLogin(){
      ipcMain.removeAllListeners('login');
      ipcMain.removeAllListeners('guard');
      modal.close();
    }
  });

  modal.loadURL(url.format({
    pathname: path.join(__dirname, 'pages/login.html'),
    protocol: 'file:',
    slashes: true
  }));
}

function createSplash(){
  var modal = new BrowserWindow({
    width: 550,
    height: 200,
    frame: false,
    show: false,
    backgroundColor: '#fff',
    resizable: false,
    movable: false,
    minimizable: false,
    maximizable: false,
    title: "Dreamcast 2"
  });


  modal.on('ready-to-show', () => {
    modal.show();
    
    steam.on('progress', (data) => {
      modal.webContents.send('progress',  data);
    });
    steam.on('ready', () => {
      console.log('steam ready');
      steam.getInfo().then((info) => {
        installPath = info.InstallPath;
        if (info.Account == "" || info.SteamID == 0) {
          createLogin();
        }else{
          modal.webContents.send('progress', {
            text: `Logging in ${info.Account}`,
            value: 0
          });
          steam.login(info.Account).then(() => {
            createMainWindow();
          }, () => {
            createLogin();
          });
        }
        steam.removeAllListeners('progress');
        steam.removeAllListeners('ready');
        modal.close();
      });
    });
    steam.init();
  });
  modal.loadURL(url.format({
    pathname: path.join(__dirname, 'pages/splash.html'),
    protocol: 'file:',
    slashes: true
  }));
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
// app.on('ready', createLogin)
app.on('ready', createSplash)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.